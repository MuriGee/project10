//
//  PersonCell.swift
//  Project10
//
//  Created by Muri Gumbodete on 29/03/2022.
//

import UIKit

class PersonCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
    
}
